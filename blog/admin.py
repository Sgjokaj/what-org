from django.contrib import admin
from .models import Article

    
class PostAdmin(admin.ModelAdmin):    
    list_display=['title','tags']
    list_filter=['taggit']

    def tags(self, post):
        tags = []
        for tag in post.taggit.all():
            tags.append(str(tag))
        return ', '.join(tags)


admin.site.register(Article, PostAdmin,)

# Register your models here.
