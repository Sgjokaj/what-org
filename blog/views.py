from django.shortcuts import render, reverse
from django.views.generic import (
    ListView,
    DetailView,
)
from .models import Article
from taggit.models import Tag
# Create your views here.


class PostListView(ListView):
    model = Article
    template_name = 'blog/home.html'  # <app>/<model>_<viewtype>.html
    queryset=Article.objects.all()
    context_object_name = 'posts'


class PostDetailView(DetailView):
    model = Article
    template_name = 'blog/post_detail.html'


class TagIndexView(ListView):
	model = Article
	template_name = 'blog/home.html'
	context_object_name = 'posts'

	def get_queryset(self):
		return Article.objects.filter(taggit__slug=self.kwargs.get('tag_slug'))


def change_language(request):
    response = HttpResponseRedirect('/')
    if request.method == 'POST':
        language = request.POST.get('language')
        if language:
            if language != settings.LANGUAGE_CODE and [lang for lang in settings.LANGUAGES if lang[0] == language]:
                redirect_path = f'/{language}/'
            elif language == settings.LANGUAGE_CODE:
                redirect_path = '/'
            else:
                return response
            from django.utils import translation
            translation.activate(language)
            response = HttpResponseRedirect(redirect_path)
            response.set_cookie(settings.LANGUAGE_COOKIE_NAME, language)
    return response